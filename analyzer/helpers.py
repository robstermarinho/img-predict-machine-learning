from django.contrib.humanize.templatetags.humanize import naturaltime
from django.core import serializers
from django.http import Http404, HttpResponse, JsonResponse
from django.shortcuts import get_object_or_404
from django.template.loader import render_to_string
from django.utils import http
from django.utils.html import escape
from django.contrib import humanize


# Datatable helper
class DatatableTransformations():

    def highlightBlue(text):
        return "<strong class='font-blue-soft'>" + str(text) + "</strong>"

    def highlightOrange(text):
        return "<strong class='font-yellow-casablanca'>" + str(text) + "</strong>"

    def printSlug(text):
        return "<small class='font-blue-soft'>" + str(text) + "</small>"

    def generateHeaderCheckBox(table_id):
        return '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input type="checkbox" class="group-checkable" data-set="' + str(
            table_id) + ' .checkboxes" /><span></span></label>'

    def generateRowCheckBox(item_id):
        return '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input type="checkbox" class="checkboxes" value="' + str(
            item_id) + '" /><span></span></label>'

    def generateNaturalTime(date, bg_color='bg-blue-madison bg-font-blue-madison'):
        return '<span style="font-size:13px;" class="label ' + str(
            bg_color) + '"></i> ' + naturaltime(date) + '</span>'

    def generateDateLabel(date, bg_color='bg-blue-madison bg-font-blue-madison'):
        return '<span style="font-size:13px;" class="label ' + str(
            bg_color) + '"> ' + escape("{:%m/%d/%Y}".format(date)) + '</span>'

    def generateDateHourLabel(date, display_hour=True, bg_color='bg-blue-madison bg-font-blue-madison'):
        dt = '<span style="font-size:13px;" class="label ' + str(
            bg_color) + '"> ' + escape("{:%m/%d/%Y}".format(date)) + '</span>'

        hour = '<span style="font-size:13px;" class="label bg-yellow-casablanca"><i class="fa fa-clock-o"></i> ' + escape(
            "{:%H:%M:%S}".format(date)) + '</span>'

        if display_hour:
            return dt + hour
        return dt


# Ajax Mixin
class AJAXMixin(object):

    def handle_no_permission(self):
        data = {}
        data['msg'] = 'You have no sufficient permissions <br>'
        data['msg'] += '<small class="font-red-flamingo">' + str(self.model.__name__) + ' - ' + str(
            self.permission_required) + '</small>'
        return JsonResponse(data, status=403)

    def dispatch(self, request, *args, **kwargs):
        if not request.is_ajax():
            raise Http404("This is an ajax view, friend.")
        return super(AJAXMixin, self).dispatch(request, *args, **kwargs)


# Ajax Response Mixin to generate custom Ajax responses for the class based views
class AjaxResponseMixin:

    # Send AJAX response when the User has no permission
    def handle_no_permission(self):
        data = {}
        data['msg'] = 'You have no sufficient permissions <br>'
        data['msg'] += '<small class="font-red-flamingo">' + str(self.model.__name__) + ' - ' + str(
            self.permission_required) + '</small>'
        return JsonResponse(data, status=403)

    # After submit some POST/GET form save it and return Json response
    def saveAjaxForm(self, form):
        data = {}
        status = 200

        if self.request.is_ajax() and self.request.method == 'POST':
            if form.is_valid():
                self.saveFormLogic(form)
                data['form_is_valid'] = True
                data['title'] = 'Success'
                data['msg'] = 'The ' + str(self.model.__name__) + ' has been saved!'
            else:
                data['form_is_valid'] = False
                data['form_errors'] = form.errors
                data['title'] = 'Request Fail'
                data['msg'] = 'The ' + str(self.model.__name__) + ' cannot be saved!'
                # status = 400

        context = {
            'form': form,
            'modal_title': self.modal_title,
            'request_path': self.request.path
        }

        data['html_form'] = render_to_string(self.template_name, context, request=self.request)
        return JsonResponse(data, status=status)

    # Instantiate the right form and modal title based on the request
    def actionForm(self, pk, request):
        if pk is not None:  # If it´s an update
            object = get_object_or_404(self.model, pk=pk)  # Find the object by 'pk'
            self.modal_title += 'Update '  # Change the modal title to the group name
            self.modal_title += str(self.model.__name__)
            self.modal_title += ' | ' + str(object)
            if self.request.method == 'GET':
                form = self.form_class(instance=object)  # Instantiate the form
            elif self.request.method == 'POST':
                form = self.form_class(data=request.POST, files=request.FILES, instance=object)  # Instantiate the form

        else:  # If it´s a creation
            self.modal_title += 'Create '  # Change the modal title
            self.modal_title += str(self.model.__name__)

            if self.request.method == 'GET':
                form = self.form_class()  # Instantiate the form
            elif self.request.method == 'POST':
                form = self.form_class(data=request.POST, files=request.FILES)

        return self.saveAjaxForm(form)  # Save the form and send the AJAX response

    def get(self, request, pk=None, *args, **kwargs):  # Triggered after GET method
        return self.actionForm(pk, request)

    def post(self, request, pk=None, *args, **kwargs):  # Triggered after POST method
        return self.actionForm(pk, request)

# Ajax Mixin
class AJAXLoadMixin(object):

    def loadAJAXResponse(self):  # Triggered after GET method
        data = {
            'msg' : '',
            'html_form': '',
            'title': '',
            'status': ''
        }
        status = 200

        context = {
            'modal_title': self.modal_title,
            'request_path': self.request.path
        }

        if self.add_context and len(self.add_context) > 0:
            for key, value in self.add_context.items():
                context[key] = value

        if self.object:
            context['object'] = self.object


        if 'msg' in context:
            data['msg'] = context['msg']
        if 'message' in context:
            data['message'] = context['message']
        if 'error' in context:
            data['error'] = context['error']
        if 'form_is_valid' in context:
            data['form_is_valid'] = context['form_is_valid']
        if 'title' in context:
            data['title'] = context['title']
        if 'status' in context:
            data['status'] = context['status']

        data['html_form'] = render_to_string(self.template_name, context, request=self.request)

        return JsonResponse(data, status=status)


# Remove a list of ids and return a JSON response with errors and warnings for each object
class RemoveObjectsMixin:

    def post(self, request, *args, **kwargs):
        list_of_ids = request.POST.getlist('items[]', None)
        comp_warnings = []
        comp_errors = []
        items_removed = 0

        for id in list_of_ids:
            object = self.model.objects.get(id=id)
            deleted = self.deleteObject(object)
            if deleted[0] is None:
                items_removed += 1
            elif deleted[0] == 'warning':
                comp_warnings.append(deleted[1])
            elif deleted[0] == 'error':
                comp_warnings.append(deleted[1])

        data = {
            'status': items_removed > 0,
            'removed': items_removed,
            'comp_errors': comp_errors,
            'comp_warnings': comp_warnings,
        }

        return JsonResponse(data, status=200)



class AJAXDropdownMixin:

    def post(self, request, pk=None, *args, **kwargs):
        find = request.POST.get('q', None)
        result = self.query(find, pk)
        return JsonResponse(result, status=200, safe=False)