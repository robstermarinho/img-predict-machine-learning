from django.conf.urls.static import static
from django.urls import path

from imganalyzer import settings
from . import views
from analyzer import views as analyzer_views

app_name = 'analyzer'

urlpatterns = [

    # Home
    path('', analyzer_views.Home.as_view(), name='home'),

    # Images
    path('image/create', analyzer_views.ImageForm.as_view(), name='image_create'),
    path('image/list', analyzer_views.ImageList.as_view(), name='image_list'),
    path('datatable/image/<pk>/categories', views.ImageCategoriesDatatable.as_view(), name='image_categories_dt'),
    path('image/<pk>/category/add', views.ImageCategoryAdd.as_view(), name='image_category_add'),
    path('image/<pk>/category/remove', views.ImageCategoryRemove.as_view(), name='image_category_remove'),

    # Category
    path('category/create', analyzer_views.CategoryForm.as_view(), name='category_create'),
    path('category/list', analyzer_views.CategoryList.as_view(), name='category_list'),
    path('image/<pk>/categories/update', analyzer_views.ImageCategoriesUpdate.as_view(), name='image_cat_update'),
    path('category/ajax_list', analyzer_views.CategoryAjaxList.as_view(), name='category_ajax_list'),

    path('datatable/categories', analyzer_views.CategoriesDatatable.as_view(), name='categories_dt'),
    path('categories', analyzer_views.CategoriesListView.as_view(), name='categories'),
    path('categories/remove', analyzer_views.CategoriesRemove.as_view(), name='categories_remove'),
    path('category/update/<pk>', analyzer_views.CategoryForm.as_view(), name='category_update'),

    path('predict/image/<pk>', analyzer_views.PredictImageCategory.as_view(), name='predict_image'),
]


if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
