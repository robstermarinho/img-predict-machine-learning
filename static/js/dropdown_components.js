var DropDownComponents = function () {

    var initDefaultAjaxDropDown = function (url, placeholder, element, pk, parent) {

        $.fn.select2.defaults.set("theme", "bootstrap");
        pk = typeof pk != "undefined" ? pk : null;

        function formatRepo(self) {
            if (self.loading) return self.text;

            var markup = "<div >" + self.name + "</div>";

            return markup;
        }

        function formatRepoSelection(self) {
            return self.name || self.text;
        }
        search_obj = {
            width: "off",
            allowClear: true,
            ajax: {
                url: url,
                dataType: 'json',
                delay: 250,
                type: 'post',
                data: function (params) {
                    return {
                        q: params.term,
                        csrfmiddlewaretoken: csrftoken,
                        pk: pk
                    };
                },
                processResults: function (data, page) {
                    return {
                        results: data
                    };
                },
                cache: true
            },
            escapeMarkup: function (markup) {
                return markup;
            }, // let our custom formatter work
            minimumInputLength: 0,
            placeholder: placeholder,
            templateResult: formatRepo,
            templateSelection: formatRepoSelection
        };

        $(".js-data-example-ajax").select2();

        if (typeof parent != 'undefined')
            search_obj.dropdownParent = $(parent);


        $(element).select2(search_obj);

    };

    return {
        initDefaultAjaxDropDown: initDefaultAjaxDropDown
    };

}();