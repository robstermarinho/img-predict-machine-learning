import os

from django.contrib.auth.models import User
from django.db import models
from django.urls import reverse
from django.utils.text import slugify


class Category(models.Model):
    title = models.CharField(max_length=500)
    slug = models.SlugField(max_length=500, unique=True)
    description = models.TextField(null=True, blank=True)

    def _generate_unique_slug(self):
        slug = slugify(self.title)
        unique_slug = slug
        i = 1
        while Category.objects.filter(slug=unique_slug).exists():
            unique_slug = '{}-{}'.format(slug, i)
            i += 1
        return unique_slug

    def __str__(self):
        return self.title


class Image(models.Model):
    STATUS_CHOICES = (
        ('fake', 'Fake'),
        ('real', 'Real'),
        ('not_analyzed', 'Not Analyzed')
    )

    title = models.CharField(max_length=500)
    slug = models.SlugField(max_length=500, unique=True)
    created_by = models.ForeignKey(User, on_delete=models.CASCADE, related_name='analyzer_images')
    description = models.TextField(null=True, blank=True)
    image = models.ImageField(upload_to='images')
    size = models.IntegerField()
    extension = models.CharField(max_length=10)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    status = models.CharField(max_length=15, choices=STATUS_CHOICES, default='not_analyzed')
    categories = models.ManyToManyField(Category)

    def _get_extension(self):
        name, extension = os.path.splitext(self.image.name)
        return extension

    # It Generates a unique slug based on the existent Images in database
    def _generate_unique_slug(self):
        slug = slugify(self.title)
        unique_slug = slug
        i = 1
        while Image.objects.filter(slug=unique_slug).exists():
            unique_slug = '{}-{}'.format(slug, i)
            i += 1
        return unique_slug

    def save_model(self, request, obj, form, change):
        if not obj.id:
            obj.created_by = request.user
            obj.slug = obj._generate_unique_slug()
            obj.extension = obj.image._get_extension()
        super().save_model(request, obj, form, change)

    def save(self, *args, **kwargs):
        if not self.size and self.image.size > 0:
            self.size = self.image.size
        else:
            self.size = 0
        super().save(*args, **kwargs)

    def __str__(self):
        return self.title
