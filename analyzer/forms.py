from django import forms
from django.contrib.auth.models import Group

from analyzer.models import Image, Category


class ImageForm(forms.ModelForm):

    class Meta:
        model = Image
        fields = ('image','title','description',)

class CategoryForm(forms.ModelForm):
    class Meta:
        model = Category
        fields = ('title', 'description',)