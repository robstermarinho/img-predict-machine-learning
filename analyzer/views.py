from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.contrib.auth.views import PasswordContextMixin
from django.shortcuts import render, get_object_or_404
from django.template.loader import render_to_string
from django.views.generic import TemplateView, FormView, ListView, DetailView

# Home Template View
from django.views.generic.base import View
from django_datatables_view.base_datatable_view import BaseDatatableView

from analyzer.forms import ImageForm, CategoryForm
from analyzer.helpers import AjaxResponseMixin, AJAXLoadMixin, AJAXDropdownMixin, AJAXMixin, RemoveObjectsMixin
from analyzer.models import Category, Image
from analyzer.helpers import DatatableTransformations as dt_helper

from keras.models import load_model
import argparse
import pickle
import cv2
from keras import backend as K


class Home(LoginRequiredMixin, TemplateView):
    template_name = 'dashboard/home/home.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['user_info'] = self.request.user
        context['category_count'] = Category.objects.count()
        context['images_count'] = Image.objects.count()
        return context

    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)


class PasswordResetDoneView(PasswordContextMixin, TemplateView):
    template_name = 'registration/login.html'
    title = ('Password reset sent')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['password_reset_done'] = True
        return context


class ImageForm(LoginRequiredMixin, AjaxResponseMixin, View):
    template_name = 'dashboard/image/image_form.html'  # Form modal template
    modal_title = ''  # Modal title that should be displayed
    model = Image  # Model of this Class based view
    form_class = ImageForm  # Form of this Model

    def saveFormLogic(self, form):
        img = form.save(commit=False)
        img.created_by = self.request.user
        img.slug = img._generate_unique_slug()
        img.extension = img._get_extension()
        if not img.size and img.image.size > 0:
            img.size = img.image.size
        else:
            img.size = 0
        img.save()
        return img


class CategoryForm(LoginRequiredMixin, AjaxResponseMixin, FormView):
    template_name = 'dashboard/image/category_form.html'  # Form modal template
    modal_title = ''  # Modal title that should be displayed
    model = Category  # Model of this Class based view
    form_class = CategoryForm  # Form of this Model

    def saveFormLogic(self, form):
        category = form.save(commit=False)
        category.slug = category._generate_unique_slug()
        category.save()
        return category


class ImageList(LoginRequiredMixin, ListView):
    template_name = 'dashboard/image/image_list.html'
    model = Image
    paginate_by = 10

    def get(self, request, *args, **kwargs):
        if not request.GET._mutable:
            request.GET._mutable = True
        request.GET['page'] = request.GET['block']

        return super().get(self, request, *args, **kwargs)


class CategoryList(LoginRequiredMixin, ListView):
    template_name = 'dashboard/image/category_list.html'
    model = Category
    paginate_by = 50


class ImageCategoriesUpdate(LoginRequiredMixin, AJAXLoadMixin, DetailView):
    model = Image
    template_name = 'dashboard/image/image_categories_update.html'
    modal_title = 'Image Categories'
    add_context = {}
    object = None

    def get(self, request, pk=None, *args, **kwargs):
        if pk is not None:
            self.object = get_object_or_404(self.model, pk=pk)
            self.add_context = {}
        return self.loadAJAXResponse()


class CategoryAjaxList(LoginRequiredMixin, AJAXDropdownMixin, ListView):
    model = Category

    def query(self, find, pk):

        if find is not None:
            object_list = self.model.objects.filter(title__contains=find).order_by('-id')[:20]
        else:
            object_list = self.model.objects.order_by('-id')[:20]

        result = list()

        for item in object_list:
            result.append({
                'id': item.id,
                'name': item.title
            })
        return result


class ImageCategoriesDatatable(LoginRequiredMixin, BaseDatatableView):
    model = Category
    columns = ['title', 'actions']
    max_display_length = 500

    def get_initial_queryset(self):
        pk = self.kwargs['pk']
        if not pk:
            raise NotImplementedError("Need to provide a model or implement get_initial_queryset!")
        image = Image.objects.get(pk=pk)
        return self.model.objects.filter(image__exact=image)

    def render_column(self, row, column):
        pk = self.kwargs['pk']
        image = Image.objects.get(pk=pk)
        if column == 'title':
            return dt_helper.highlightBlue(row.title)
        elif column == 'actions':
            return render_to_string('dashboard/image/image_categories_dt_buttons.html',
                                    {"category": row, 'image': image})
        else:
            return super(ImageCategoriesDatatable, self).render_column(row, column)


class ImageCategoryRemove(LoginRequiredMixin, PermissionRequiredMixin, AJAXLoadMixin, DetailView):
    permission_required = 'foundation.change_account'
    model = Image
    template_name = 'dashboard/image/image_categories_update.html'
    modal_title = 'Image Categories'
    add_context = {}
    object = None

    def post(self, request, pk=None, *args, **kwargs):
        if pk is not None:
            image = get_object_or_404(self.model, pk=pk)
            category_id = self.request.POST.get('category_id')
            category = Category.objects.get(id=category_id)
            cnt = image.categories.count()
            image.categories.remove(category)
            if image.categories.count() < cnt:
                self.add_context['status'] = True
                self.add_context['title'] = 'Success'
                self.add_context['msg'] = 'Category ' + str(category.title) + ' has been removed'
            else:
                self.add_context['status'] = False
                self.add_context['title'] = 'Fail'
                self.add_context['msg'] = 'Impossible to remove ' + str(category.title) + ''

            self.object = image

        return self.loadAJAXResponse()


class ImageCategoryAdd(LoginRequiredMixin, PermissionRequiredMixin, AJAXLoadMixin, DetailView):
    permission_required = 'foundation.change_account'
    model = Image
    template_name = 'dashboard/image/image_categories_update.html'
    modal_title = 'Image Categories'
    add_context = {}
    object = None

    def post(self, request, pk=None, *args, **kwargs):
        if pk is not None:
            image = get_object_or_404(self.model, pk=pk)
            category_id = self.request.POST.get('category_id')
            category = Category.objects.get(id=category_id)

            if category in image.categories.all():
                self.add_context['status'] = False
                self.add_context['title'] = 'Fail'
                self.add_context['msg'] = 'This category is already included!'
            else:
                cnt = image.categories.count()
                image.categories.add(category)
                if image.categories.count() > cnt:
                    self.add_context['status'] = True
                    self.add_context['title'] = 'Success'
                    self.add_context['msg'] = 'Category ' + str(category.title) + ' has been included!'
                else:
                    self.add_context['status'] = False
                    self.add_context['title'] = 'Fail'
                    self.add_context['msg'] = 'Impossible to include ' + str(category.title)

            self.object = image

        return self.loadAJAXResponse()


class CategoriesDatatable(LoginRequiredMixin, BaseDatatableView):
    model = Category
    columns = ['check',
               'title',
               'slug',
               'description',
               'actions']
    order_columns = columns
    max_display_length = 500

    def render_column(self, row, column):
        if column == 'check':
            return dt_helper.generateRowCheckBox(row.id)
        if column == 'title':
            return dt_helper.highlightOrange(row.title)
        if column == 'slug':
            return dt_helper.printSlug(row.slug)
        elif column == 'actions':
            return render_to_string('dashboard/category/partial_datatable_buttons.html', {"category": row})
        else:
            return super(CategoriesDatatable, self).render_column(row, column)


class CategoriesListView(LoginRequiredMixin, TemplateView):
    template_name = 'dashboard/category/categories.html'


class CategoriesRemove(LoginRequiredMixin, AJAXMixin, RemoveObjectsMixin, View):
    model = Category

    # Deletion Logic
    def deleteObject(self, object):
        name = '<b>' + object.title + '</b> - '
        if object.delete():
            return (None, None)
        else:
            return ('error', name + 'Impossible to remove')


class PredictImageCategory(LoginRequiredMixin, TemplateView):
    template_name = 'dashboard/home/predict.html'

    def get(self, request, *args, **kwargs):
        results = {}

        if 'pk' in kwargs:

            # try:

            img = Image.objects.get(pk=kwargs['pk'])
            width = 64
            height = 64
            flatten = -1
            model = 'keras/animals/model.model'
            label_bin = 'keras/animals/model_labels.pickle'
            predictions, label = self.predict_category(img, width, height, flatten, model, label_bin)

            results['preds'] = predictions
            results['max'] = label
            results['status'] = True
            results['img'] = img
            # except Exception as e:
            #    results['status'] = False
            #    results['msg'] = str(e)

            self.set_the_categories(img,results)
        else:
            results['status'] = False
            results['msg'] = 'Image not found!'

        context = self.get_context_data(**kwargs)
        context['results'] = results
        return self.render_to_response(context)

    def set_the_categories(self, image, results):
        image.categories.remove()

        for label in results['preds'].keys():
            if results['max'] == label:
                cat, created = Category.objects.get_or_create(
                    title=label,
                    slug=label,
                )
                image.categories.add(cat)

    def predict_category(self, img, width, height, flatten, model, label_bin):
        print([img, width, height, flatten, model, label_bin])
        # load the input image and resize it to the target spatial dimensions
        image = cv2.imread(img.image.path)
        # output = image.copy()
        image = cv2.resize(image, (width, height))

        # scale the pixel values to [0, 1]
        image = image.astype("float") / 255.0

        # check to see if we should flatten the image and add a batch
        # dimension
        if flatten > 0:
            image = image.flatten()
            image = image.reshape((1, image.shape[0]))

        # otherwise, we must be working with a CNN -- don't flatten the
        # image, simply add the batch dimension
        else:
            image = image.reshape((1, image.shape[0], image.shape[1],
                                   image.shape[2]))

        model = load_model(model)
        lb = pickle.loads(open(label_bin, "rb").read())

        # make a prediction on the image
        preds = model.predict(image)

        # find the class label index with the largest corresponding
        # probability
        i = preds.argmax(axis=1)[0]
        label = lb.classes_[i]

        predictions = preds[0]

        results = {}
        for i in range(len(predictions)):
            results[lb.classes_[i]] = round(predictions[i] * 100, 2)

        # results['max'] = label

        del model
        del image
        del predictions
        del lb
        K.clear_session()

        return [results, label]
