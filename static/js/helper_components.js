var HelperComponents = function () {


    var showAjaxError = function (data, modal) {
        var text = JSON.stringify(data.responseText).substr(0, 400) + '...';
        var title = "Request failed";
        if (data.status = 403) {
            title = "Forbidden";
            if (typeof data.responseJSON != 'undefined')
                text = data.responseJSON.msg
        }


        if (typeof modal != 'undefined')
            modal.modal("hide");

        swal({
            title: title,
            text: text,
            type: "error",
            allowOutsideClick: true,
            showConfirmButton: false,
            showCancelButton: true,
            confirmButtonClass: "",
            cancelButtonClass: "btn-danger",
            closeOnConfirm: false,
            closeOnCancel: true,
            confirmButtonText: "",
            cancelButtonText: "OK",
            html: true
        });
    };

    var showAjaxErrorAlert = function (data, container, modal) {
        var text = JSON.stringify(data.responseText).substr(0, 400) + '...';
        var title = "Request failed";
        if (data.status = 403) {
            title = "Forbidden";
            if (typeof data.responseJSON != 'undefined')
                text = data.responseJSON.msg
        }


        if (typeof modal != 'undefined')
            modal.modal("hide");

        App.alert({
            container: container.closest('.portlet-body'),
            place: "preppend",
            type: "danger",
            message: text,
            close: true,
            reset: false,
            focus: false,
            closeInSeconds: 3,
            icon: 'fa fa-exclamation-triangle'
        });
    };

    var loadForm = function (e) {
        e.preventDefault();
        var btn = $(this);
        var modal = $(btn.attr("data-modal"));

        $.ajax({
            url: btn.attr("data-url"),
            type: 'GET',
            dataType: 'json',
            beforeSend: function () {
                App.blockUI({
                    target: btn.closest('.portlet'),
                    animate: true,
                    overlayColor: 'none'
                });
            },
            complete: function () {
                App.unblockUI(btn.closest('.portlet'));
            },

            success: function (data) {
                modal.find('.modal-content').html(data.html_form);
                modal.modal("show");
            },
            error: function (data) {
                showAjaxError(data, modal);
            },
        });
    };

    var saveForm = function (e) {
        e.preventDefault();

        var form = $(this);
        var modal = $('#general_modal');
        var datatable = window.DjangoDataTables["datatable"];

        $.ajax({
            url: form.attr("action"),
            data: form.serialize(),
            type: form.attr("method"),
            dataType: 'json',
            complete: function () {
                App.unblockUI(modal.find('.modal-content'));
            },
            beforeSend: function () {
                App.blockUI({
                    target: modal.find('.modal-content'),
                    animate: true,
                    overlayColor: 'none'
                });
            },
            success: function (data) {
                if (data.form_is_valid) {
                    modal.modal("hide");
                    datatable.draw(false);

                    swal({
                        title: data.title,
                        text: data.msg,
                        type: "success",
                        allowOutsideClick: true,
                        showConfirmButton: false,
                        showCancelButton: true,
                        confirmButtonClass: "",
                        cancelButtonClass: "btn-success",
                        closeOnConfirm: false,
                        closeOnCancel: true,
                        confirmButtonText: "",
                        cancelButtonText: "OK",
                    });
                } else {


                    modal.find(".modal-content").html(data.html_form);
                    App.alert({
                        container: modal.find('.modal-body'),
                        place: "preppend",
                        type: "danger",
                        message: "Invalid data. Please correct it before submit.",
                        close: true,
                        reset: false,
                        focus: true,
                        closeInSeconds: 10,
                        icon: 'fa fa-exclamation-triangle'
                    });
                }
            },
            error: function (data) {
                showAjaxError(data, modal);
            },
        });
        return false;
    };

    var saveFormWithFile = function (e) {
        e.preventDefault();

        var form = $(this);
        var modal = $('#general_modal');
        if (typeof window.DjangoDataTables != 'undefined')
            var datatable = window.DjangoDataTables["datatable"];

        formData = new FormData(form[0]);


        $.ajax({
            url: form.attr("action"),
            data: formData,
            type: form.attr("method"),
            dataType: 'json',
            contentType: false,
            processData: false,
            complete: function () {
                App.unblockUI(modal.find('.modal-content'));
                $('.btn_confirm').prop('disabled', false);
                $('.btn_cancel').show().prop('disabled', false);
            },
            beforeSend: function () {
                App.blockUI({
                    target: modal.find('.modal-content'),
                    customEMX: true,
                    overlayColor: '#666'
                });
                $(".modal-footer .percent_progress").show();
                $(" .percent_label").text(0 + '%');
                $('.btn_confirm').prop('disabled', true);
                $('.btn_cancel').prop('disabled', true).hide();
            },
            success: function (data) {
                if (data.form_is_valid) {
                    modal.modal("hide");
                    if (typeof datatable != 'undefined')
                        datatable.draw(false);

                    swal({
                        title: data.title,
                        text: data.msg,
                        type: "success",
                        allowOutsideClick: true,
                        showConfirmButton: false,
                        showCancelButton: true,
                        confirmButtonClass: "",
                        cancelButtonClass: "btn-success",
                        closeOnConfirm: false,
                        closeOnCancel: true,
                        confirmButtonText: "",
                        cancelButtonText: "OK",
                    });
                } else {


                    modal.find(".modal-content").html(data.html_form);
                    App.alert({
                        container: modal.find('.modal-body'),
                        place: "preppend",
                        type: "danger",
                        message: "Invalid data. Please correct it before submit.",
                        close: true,
                        reset: false,
                        focus: true,
                        closeInSeconds: 10,
                        icon: 'fa fa-exclamation-triangle'
                    });
                }
            },
            error: function (data) {
                showAjaxError(data, modal);
            },
            xhr: function () {
                var jqXHR = null;
                if (window.ActiveXObject) {
                    jqXHR = new window.ActiveXObject("Microsoft.XMLHTTP");
                } else {
                    jqXHR = new window.XMLHttpRequest();
                }
                //Upload progress
                jqXHR.upload.addEventListener("progress", function (evt) {
                    if (evt.lengthComputable) {
                        var percentComplete = Math.round((evt.loaded * 100) / evt.total);
                        $(".modal-footer .progress-bar").css('width', percentComplete + '%').attr('aria-valuenow', percentComplete);
                        console.log('Uploaded percent', percentComplete);
                        $(" .percent_label").text(percentComplete + '%');
                    }
                }, false);
                //Download progress
                jqXHR.addEventListener("progress", function (evt) {
                    if (evt.lengthComputable) {
                        var percentComplete = Math.round((evt.loaded * 100) / evt.total);
                    }
                }, false);
                return jqXHR;
            },
        });

        return false;
    };

    var removeSetAsk = function (e) {
        e.preventDefault();
        var set_to_remove = [];
        var btn = $(this);
        var set = $(this).data("set");
        var url = $(this).data("url");

        $(set).each(function () {
            if ($(this).is(":checked")) set_to_remove.push($(this).val());
        });

        if (set_to_remove.length == 0) {
            swal({
                title: "Nothing Selected",
                text: "No items has been selected!",
                type: "error",
                allowOutsideClick: true,
                showConfirmButton: false,
                showCancelButton: true,
                confirmButtonClass: "",
                cancelButtonClass: "btn-success",
                closeOnConfirm: false,
                closeOnCancel: true,
                confirmButtonText: "",
                cancelButtonText: "OK",
            });
        } else {
            var title = set_to_remove.length > 1 ? " items selected" : " item selected";
            swal({
                title: set_to_remove.length + title,
                text: "Do you want to remove the " + title + "?",
                type: "info",
                allowOutsideClick: true,
                showConfirmButton: true,
                showCancelButton: true,
                confirmButtonClass: "btn-danger",
                cancelButtonClass: "btn-success",
                closeOnConfirm: false,
                closeOnCancel: true,
                confirmButtonText: "Remove",
                cancelButtonText: "Cancel",
            }, function (isConfirm) {
                if (isConfirm) {
                    removeSetOfItems(btn, set_to_remove, url);
                }
            });
        }
    };

    var removeSetOfItems = function (btn, set_to_remove, url) {
        var datatable = window.DjangoDataTables["datatable"];

        $.ajax({
            url: url,
            method: "POST",
            data: {
                'items': set_to_remove,
                'csrfmiddlewaretoken': csrftoken
            },
            dataType: "json",
            beforeSend: function () {
                App.blockUI({
                    target: btn.closest('.portlet'),
                    animate: true,
                    overlayColor: 'none'
                });
            },
            complete: function () {
                App.unblockUI(btn.closest('.portlet'));
            },
            success: function (data) {

                if (data.comp_errors.length > 0) {
                    data.comp_errors.forEach(function (element) {
                        App.alert({
                            container: btn.closest('.portlet'),
                            place: "preppend",
                            type: "danger",
                            message: element,
                            close: true,
                            reset: false,
                            focus: true,
                            closeInSeconds: 10,
                            icon: 'fa fa-exclamation-triangle'
                        });
                    });
                }
                if (data.comp_warnings.length > 0) {
                    data.comp_warnings.forEach(function (element) {
                        App.alert({
                            container: btn.closest('.portlet'),
                            place: "preppend",
                            type: "warning",
                            message: element,
                            close: true,
                            reset: false,
                            focus: true,
                            closeInSeconds: 10,
                            icon: 'fa fa-exclamation-circle'
                        });
                    });
                }

                if (data.status) {
                    var title = data.removed > 1 ? " items" : " item";
                    var title2 = data.removed > 1 ? " items has been" : " item has been";

                    swal({
                        title: data.removed + title + " removed",
                        text: data.removed + title2 + " successfully removed!",
                        type: "success",
                        allowOutsideClick: true,
                        showConfirmButton: false,
                        showCancelButton: true,
                        confirmButtonClass: "",
                        cancelButtonClass: "btn-success",
                        closeOnConfirm: false,
                        closeOnCancel: true,
                        confirmButtonText: "",
                        cancelButtonText: "OK",
                    });

                    datatable.draw(false);
                } else {
                    swal({
                        title: "0 Removed",
                        text: "Items could not be removed",
                        type: "error",
                        allowOutsideClick: true,
                        showConfirmButton: false,
                        showCancelButton: true,
                        confirmButtonClass: "",
                        cancelButtonClass: "btn-danger",
                        closeOnConfirm: false,
                        closeOnCancel: true,
                        confirmButtonText: "",
                        cancelButtonText: "OK",
                    });
                }
            },
            error: function (data) {
                showAjaxError(data);
            },
        });
    };

    var loadDashboardPortfolio = function () {
        $('#js-images').cubeportfolio({
            filters: '#js-image-filters',
            loadMore: '#js-images-load-more',
            loadMoreAction: 'click',
            layoutMode: 'grid',
            defaultFilter: '*',
            animationType: 'quicksand',
            gapHorizontal: 35,
            gapVertical: 30,
            gridAdjustment: 'responsive',
            mediaQueries: [{
                width: 1500,
                cols: 5
            }, {
                width: 1100,
                cols: 4
            }, {
                width: 800,
                cols: 3
            }, {
                width: 480,
                cols: 2
            }, {
                width: 320,
                cols: 1
            }],
            caption: 'overlayBottomReveal',
            displayType: 'sequentially',
            displayTypeSpeed: 80,

            // lightbox
            lightboxDelegate: '.cbp-lightbox',
            lightboxGallery: true,
            lightboxTitleSrc: 'data-title',
            lightboxCounter: '<div class="cbp-popup-lightbox-counter">{{current}} of {{total}}</div>',

            // singlePage popup
            singlePageDelegate: '.cbp-singlePage',
            singlePageDeeplinking: true,
            singlePageStickyNavigation: true,
            singlePageCounter: '<div class="cbp-popup-singlePage-counter">{{current}} of {{total}}</div>',
            singlePageCallback: function (url, element) {
                // to update singlePage content use the following method: this.updateSinglePage(yourContent)
                var t = this;

                $.ajax({
                    url: url,
                    type: 'GET',
                    dataType: 'html',
                    beforeSend: function () {
                        App.blockUI({
                            target: $('body'),
                            animate: true,
                            overlayColor: 'none'
                        });
                    },
                    complete: function () {
                        App.unblockUI($('body'));
                    },

                    /*timeout: 10000*/
                })
                    .done(function (result) {
                        console.log(result);
                        t.updateSinglePage(result);
                    })
                    .fail(function () {
                        t.updateSinglePage('AJAX Error! Please refresh the page!');
                    });
            },
        });
    };

    var addItemAJAX = function (id, btn, data, field_name, datatable) {
        if (typeof id != 'undefined' && id != '') {
            $.ajax({
                url: btn.data("url"),
                data: data,
                type: 'POST',
                dataType: 'json',
                beforeSend: function () {
                    $('.loading_spinning').show();
                },
                complete: function () {
                    $('.loading_spinning').hide();
                },
                success: function (data) {
                    if (data.status) {
                        datatable.draw(false);
                        toastr.success(data.msg,data.title);
                        toastr.options = {
                            "closeButton": true,
                            "debug": false,
                            "progressBar": false,
                            "positionClass": "toast-top-right",
                            "onclick": null,
                            "showDuration": "300",
                            "hideDuration": "1000",
                            "timeOut": "5000",
                            "extendedTimeOut": "1000",
                            "showEasing": "swing",
                            "hideEasing": "linear",
                            "showMethod": "fadeIn",
                            "hideMethod": "fadeOut"
                        }

                    } else {
                        datatable.draw(false);

                        toastr.error(data.msg,data.title);
                        toastr.options = {
                            "closeButton": true,
                            "debug": false,
                            "progressBar": false,
                            "positionClass": "toast-top-right",
                            "onclick": null,
                            "showDuration": "300",
                            "hideDuration": "1000",
                            "timeOut": "5000",
                            "extendedTimeOut": "1000",
                            "showEasing": "swing",
                            "hideEasing": "linear",
                            "showMethod": "fadeIn",
                            "hideMethod": "fadeOut"
                        }
                    }
                },
                error: function (data) {
                    var text = JSON.stringify(data.responseText).substr(0, 400) + '...';
                    var title = 'Request failed';
                    toastr.error(title, text);
                    toastr.options = {
                        "closeButton": true,
                        "debug": false,
                        "progressBar": false,
                        "positionClass": "toast-top-right",
                        "onclick": null,
                        "showDuration": "300",
                        "hideDuration": "1000",
                        "timeOut": "5000",
                        "extendedTimeOut": "1000",
                        "showEasing": "swing",
                        "hideEasing": "linear",
                        "showMethod": "fadeIn",
                        "hideMethod": "fadeOut"
                    }
                },
            });
        } else {
            var text = 'Please, the ' + field_name + ' cannot be empty';
            var title = 'Empty';
            toastr.error(title, text);
            toastr.options = {
                "closeButton": true,
                "debug": false,
                "progressBar": false,
                "positionClass": "toast-top-right",
                "onclick": null,
                "showDuration": "300",
                "hideDuration": "1000",
                "timeOut": "5000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            }
        }
    };

    var removeItemAJAX = function (id, btn, data, field_name, datatable) {

        isConfirm = confirm("Are you sure you want to remove this " + field_name + "?");
        if (isConfirm) {
            $.ajax({
                url: btn.data("url"),
                data: data,
                type: 'POST',
                dataType: 'json',
                beforeSend: function () {
                    $('.loading_spinning').show();
                },
                complete: function () {
                    $('.loading_spinning').hide();
                },
                success: function (data) {
                    if (data.status) {
                        datatable.draw(false);
                        toastr.success(data.msg,data.title);
                        toastr.options = {
                            "closeButton": true,
                            "debug": false,
                            "progressBar": false,
                            "positionClass": "toast-top-right",
                            "onclick": null,
                            "showDuration": "300",
                            "hideDuration": "1000",
                            "timeOut": "5000",
                            "extendedTimeOut": "1000",
                            "showEasing": "swing",
                            "hideEasing": "linear",
                            "showMethod": "fadeIn",
                            "hideMethod": "fadeOut"
                        }
                        //context.html(data.html_form);
                    } else {
                        datatable.draw(false);
                        //context.html(data.html_form);
                        toastr.error(data.msg,data.title);
                        toastr.options = {
                            "closeButton": true,
                            "debug": false,
                            "progressBar": false,
                            "positionClass": "toast-top-right",
                            "onclick": null,
                            "showDuration": "300",
                            "hideDuration": "1000",
                            "timeOut": "5000",
                            "extendedTimeOut": "1000",
                            "showEasing": "swing",
                            "hideEasing": "linear",
                            "showMethod": "fadeIn",
                            "hideMethod": "fadeOut"
                        }
                    }
                },
                error: function (data) {
                    var text = JSON.stringify(data.responseText).substr(0, 400) + '...';
                    var title = 'Request failed';
                    toastr.error(title, text);
                    toastr.options = {
                        "closeButton": true,
                        "debug": false,
                        "progressBar": false,
                        "positionClass": "toast-top-right",
                        "onclick": null,
                        "showDuration": "300",
                        "hideDuration": "1000",
                        "timeOut": "5000",
                        "extendedTimeOut": "1000",
                        "showEasing": "swing",
                        "hideEasing": "linear",
                        "showMethod": "fadeIn",
                        "hideMethod": "fadeOut"
                    }
                },
            });
        }


    };


    return {
        loadForm: loadForm,
        saveForm: saveForm,
        saveFormWithFile: saveFormWithFile,
        removeSetAsk: removeSetAsk,
        showAjaxError: showAjaxError,
        showAjaxErrorAlert: showAjaxErrorAlert,
        loadDashboardPortfolio: loadDashboardPortfolio,
        removeItemAJAX: removeItemAJAX,
        addItemAJAX: addItemAJAX
    };

}();