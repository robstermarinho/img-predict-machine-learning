from django.contrib import admin
from analyzer.models import Image, Category


# Register your models here.



@admin.register(Category)
class Category(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('title',)}


@admin.register(Image)
class Image(admin.ModelAdmin):
    list_display = ('title', 'slug', 'description', 'created_at', 'updated_at')
    list_filter = ('title','created_at')
    search_fields = ('title', 'slug', 'description')
    prepopulated_fields = {'slug': ('title',)}
    date_hierarchy = 'created_at'

